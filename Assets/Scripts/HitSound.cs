﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AudioSource))]

public class HitSound : MonoBehaviour, INonPickupable
{

    public AudioClip HitSoundClip;

    void OnTriggerEnter(Collider pickupable)
    {
       audio.PlayOneShot(HitSoundClip);
    }



}