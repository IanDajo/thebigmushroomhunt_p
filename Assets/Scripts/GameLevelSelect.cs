﻿using UnityEngine;
using System.Collections;

public class GameLevelSelect : MonoBehaviour {

	UIButton gameLevelButton;
	UILabel gameLevelButtonLabel;

	void OnClick() {
		GameMaster.GM.AdvanceToNextGameLevel();
		SetLabel();
	}

	// Use this for initialization
	void Start () {
		gameLevelButton = GetComponent<UIButton>();
		if (gameLevelButton == null) throw new MissingReferenceException("Can't find GameLevelButton!");

		gameLevelButtonLabel = gameLevelButton.GetComponentInChildren<UILabel>();
		if (gameLevelButtonLabel == null) throw new MissingReferenceException("Can't find gameLevelButtonLabel!");

		SetLabel();

	}

	private void SetLabel() {
		string text = GameMaster.GM.GameLevelAsString ();
		Debug.Log("set gamelevelbuttonlabel to: " + text);
		gameLevelButtonLabel.text = text;
	}

}
