﻿using UnityEngine;
using System.Collections;

public class HitParticle : MonoBehaviour, IPickupable {

    public ParticleSystem particles;


    public void Pickup()
    {
        // Debug.Log("particles");
        particles.Play();
    }


    public bool HasBeenPickedUp()
    {
        return false;
    }
}
