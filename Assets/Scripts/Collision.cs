﻿using UnityEngine;
using System.Collections;

public class Collision : MonoBehaviour {
    public Transform CollisionPoint;

    void Pickup()
    {
        var hits = Physics.OverlapSphere(CollisionPoint.position, 0.5f);

        foreach (var hit in hits)
        {
            Debug.Log(hit.name);
            var hitables = hit.GetComponents(typeof(IPickupable));

            if (hitables == null)
                return;

            foreach (IPickupable hitable in hitables)
            {
                hitable.Pickup();
            }
        }
    }
}
