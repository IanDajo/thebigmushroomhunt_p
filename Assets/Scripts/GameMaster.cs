﻿using UnityEngine;
using System.Collections;

public enum GameLevelType {
	Easy = 1,
	Intermediate = 2,
	Difficult = 3
}

[RequireComponent(typeof(AudioSource))]
public class GameMaster : MonoBehaviour {

    public static GameMaster GM;

    public AudioClip GameStartSound;
    public AudioClip GameDoneSound;
    public AudioClip GameOverSound;

    public int Score;
    public bool HasWon;

	// Use this for initialization
	void Awake () {
  //      if (GM != null)
  //      {
  //          GameObject.Destroy(GM);
  //      }
  //      else
  //      {
            GM = this;
 //       }

        DontDestroyOnLoad(this);
	}

	public GameLevelType GameLevel = GameLevelType.Intermediate;

	public void ResetGame() {
		HasWon = false;
		Score = 0;
	}

	public void AdvanceToNextGameLevel() {
		switch (GameLevel) {
				case GameLevelType.Easy:
						GameLevel = GameLevelType.Intermediate;
						break;
				case GameLevelType.Intermediate:
						GameLevel = GameLevelType.Difficult;
						break;
				case GameLevelType.Difficult:
						GameLevel = GameLevelType.Easy;
						break;
				}
	}

	public string GameLevelAsString() {
		return System.Enum.GetName(typeof(GameLevelType), GameLevel);
	}

	public void IncreaseScoreBy(int points) {
		Score = Score + points;
	}

	public void PlayGameStartSound()
	{
		audio.PlayOneShot(GameStartSound);
	}

	public void PlayGameDoneSound()
	{
		audio.PlayOneShot(GameDoneSound);
	}

	public void PlayGameOverSound()
    {
        audio.PlayOneShot(GameOverSound);
    }
}
