﻿using UnityEngine;
using System.Collections;

public class GameOverStatus : MonoBehaviour {

    UILabel wonLabel;

    // Use this for initialization
    void Start()
    {

        wonLabel = GetComponent<UILabel>();
        if (wonLabel == null)
            throw new MissingReferenceException("Can't find wonLabel!");

        if (GameMaster.GM.HasWon)
        {
            wonLabel.text = "Congratulations! You Have Won!";
                GameMaster.GM.PlayGameDoneSound();
        }
        else
        {
            wonLabel.text = "Game Over! Try again!";
                GameMaster.GM.PlayGameOverSound();
        }
    }

    // Update is called once per frame
    void Update()
    {
    }
}
