﻿using UnityEngine;
using System.Collections;

public class UpdateScoreLabel : MonoBehaviour {

	public Stats playerStats;
	public UILabel scoreLabel;
	
	// Use this for initialization
	void Start()
	{
		playerStats = GameObject.Find("Player").GetComponent<Stats>();
		
		if (playerStats == null)
			throw new MissingReferenceException("Can't find playerstats!");
		
		scoreLabel = GetComponent<UILabel>();
		
		if (scoreLabel == null)
			throw new MissingReferenceException("Can't find timebar!");
		
		scoreLabel.text = GameMaster.GM.Score.ToString();
	}
	
	// Update is called once per frame
	void Update()
	{
		scoreLabel.text = GameMaster.GM.Score.ToString();
	}
}
