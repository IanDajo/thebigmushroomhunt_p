﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Stats))]
public class PickupDetector : MonoBehaviour {
    Stats stats;

    void Start()
    {
        stats = GetComponent<Stats>();
    }

	void Update()
	{
		if (stats.IsLevelGameOver())
		{
			Application.LoadLevel("GameOver");
		}
	}

    void OnTriggerEnter(Collider hit)
    {
        Debug.Log(hit.name);
        var hitables = hit.GetComponents(typeof(IPickupable));

        var damagables = hit.GetComponents(typeof(IDamagable));
        if (hitables == null && damagables == null)
            return;

        foreach (var hitable in hitables)
        {
            Debug.Log(hitable.name);

            IPickupable pickupable = (IPickupable)hitable;
			if (!pickupable.HasBeenPickedUp()) {
            	pickupable.Pickup();
				stats.PlayerHasFoundMushroom();
			}

        } 
        
        foreach (var damagable in damagables) 
        {
            Debug.Log(damagable.name);

            IDamagable damableobject = (IDamagable)damagable;
            damableobject.Damage();

			stats.DecreaseHealth();
        }



    }
}
