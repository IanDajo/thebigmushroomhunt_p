﻿using UnityEngine;
using System.Collections;

public class TimeBar : MonoBehaviour {

    public Stats playerStats;
    public UISlider timeBar;

	// Use this for initialization
	void Start () {
        playerStats = GameObject.Find("Player").GetComponent<Stats>();

        if (playerStats == null)
            throw new MissingReferenceException("Can't find playerstats!");

        timeBar = GetComponent<UISlider>();

        if (timeBar == null)
            throw new MissingReferenceException("Can't find timebar!");

        timeBar.sliderValue = playerStats.GameTimeForProgressBar;
	}
	
	// Update is called once per frame
	void Update () {
        timeBar.sliderValue = playerStats.GameTimeForProgressBar;
	}
}
