﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Stats))]
[RequireComponent(typeof(AudioSource))]
public class DamageObject : MonoBehaviour, IDamagable {

    Stats stats;
    public AudioClip DamageSound;

    void Start()
    {
        stats = GetComponent<Stats>();
    }

    void OnTriggerEnter(Collider pickupable)
    {

    }

    public void Damage()
    {
        audio.PlayOneShot(DamageSound);
    }
}
