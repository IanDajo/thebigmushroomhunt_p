﻿using UnityEngine;
using System.Collections;

public class ScoreBar : MonoBehaviour
{

    public Stats playerStats;
    public UISlider scoreBar;

    // Use this for initialization
    void Start()
    {
        playerStats = GameObject.Find("Player").GetComponent<Stats>();

        if (playerStats == null)
            throw new MissingReferenceException("Can't find playerstats!");

        scoreBar = GetComponent<UISlider>();

        if (scoreBar == null)
            throw new MissingReferenceException("Can't find timebar!");

        scoreBar.sliderValue = playerStats.GameScoreForProgressBar;
    }

    // Update is called once per frame
    void Update()
    {
        scoreBar.sliderValue = playerStats.GameScoreForProgressBar;
    }
}
