﻿using UnityEngine;
using System.Collections;

public class HealthBar : MonoBehaviour
{

    public Stats playerStats;
    public UISlider healthBar;

    // Use this for initialization
    void Start()
    {
        playerStats = GameObject.Find("Player").GetComponent<Stats>();

        if (playerStats == null)
            throw new MissingReferenceException("Can't find playerstats!");

        healthBar = GetComponent<UISlider>();

        if (healthBar == null)
            throw new MissingReferenceException("Can't find timebar!");

        healthBar.sliderValue = playerStats.GameHealthForProgressBar;
    }

    // Update is called once per frame
    void Update()
    {
        healthBar.sliderValue = playerStats.GameHealthForProgressBar;
    }
}
