﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AudioSource))]
public class LoadMusicPlay : MonoBehaviour {

    public float Delay = 3;
    private bool isPlaying = false;

    public AudioClip GameMusic;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (Delay > 0)
        {
            Delay -= Time.deltaTime;
        }
        else if (!isPlaying)
        {
            isPlaying = true;
            audio.loop = true;
            audio.clip = GameMusic;
            audio.Play();
        }
	}
}
