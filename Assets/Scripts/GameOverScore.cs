﻿using UnityEngine;
using System.Collections;

public class GameOverScore : MonoBehaviour {

    UILabel scoreLabel;

    // Use this for initialization
    void Start()
    {
        scoreLabel = GetComponent<UILabel>();
        if (scoreLabel == null)
            throw new MissingReferenceException("Can't find scoretextvalue!");

        scoreLabel.text = GameMaster.GM.Score.ToString();

    }

    // Update is called once per frame
    void Update()
    {
        scoreLabel.text = GameMaster.GM.Score.ToString();
    }
}
