﻿using UnityEngine;
using System.Collections;

// [RequireComponent(typeof(Stats))]
[RequireComponent(typeof(AudioSource))]
public class PickupObject : MonoBehaviour, IPickupable
{
    // Stats stats;
    FadeObjectInOut fadeScript;
    public AudioClip PickupSound;
    public ParticleSystem PickupParticles;

	private bool hasBeenPickedUp = false;

    void Start()
    {
        // stats = GetComponent<Stats>();
        fadeScript = GetComponent<FadeObjectInOut>();
        // PickupParticles = GetComponent<ParticleSystem>();
    }

    void OnTriggerEnter(Collider pickupable)
    {
        
    }

    public void Pickup()
	{
		hasBeenPickedUp = true;
        if (PickupParticles != null)
        {
            PickupParticles.Play();
        }
        fadeScript.FadeOut();
        audio.PlayOneShot(PickupSound);
        Destroy(this.gameObject, 2.5f);
    }


    public bool HasBeenPickedUp()
    {
        return hasBeenPickedUp;
    }
}