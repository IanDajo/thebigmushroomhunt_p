﻿using UnityEngine;
using System.Collections;
using System.Timers;

public class Stats : MonoBehaviour
{
	//defaults
    public float MushroomsToHunt = 29;
    public float GameHealth = 100;
    public float GameTime = 120;

	private float huntedMushrooms = 0;
	private float maxTime = 120;
    private float maxHealth = 100;

	void Start()
	{
		InitialiseLevel(GameMaster.GM.GameLevel);
		GameMaster.GM.PlayGameStartSound();
	}
	
	void Update()
	{
		if (GameTime > 0)
		{
			GameTime -= Time.deltaTime;
		}

	}

	public void InitialiseLevel(GameLevelType gameLevel) {
		switch(gameLevel) {
		case GameLevelType.Easy :
			GameTime = 180;
			GameHealth = 130;
			break;
		case GameLevelType.Intermediate : 
			GameTime = 120;
			GameHealth = 100;
			break;
		case GameLevelType.Difficult:
			GameTime = 90;
			GameHealth = 70;
			break;
		}
		huntedMushrooms = 0;
		maxTime = GameTime;
		maxHealth = GameHealth;
	}

    public void PlayerHasFoundMushroom()
    {
		huntedMushrooms = huntedMushrooms + 1;
		PlayerHasFoundAllMushrooms ();

		int newPoints = System.Convert.ToInt32((int)GameMaster.GM.GameLevel * GameTime);
		newPoints = System.Convert.ToInt32(huntedMushrooms * GameTime) + newPoints;
	    GameMaster.GM.IncreaseScoreBy(newPoints);
    }

	public bool IsLevelGameOver() {
		return GameTime <= 0 || GameHealth <= 0 || PlayerHasFoundAllMushrooms();
	}

	public bool PlayerHasFoundAllMushrooms() {
		bool allFound = (huntedMushrooms >= MushroomsToHunt);
		GameMaster.GM.HasWon = allFound;

		return allFound;
	}

	public void DecreaseHealth() {
		GameHealth -= 10;
	}

    public float GameTimeForProgressBar
    {
        get { return (GameTime / maxTime); }
    }

    public float GameScoreForProgressBar
    {
        get { return (huntedMushrooms / MushroomsToHunt); }
    }

    public float GameHealthForProgressBar
    {
        get { return (GameHealth / maxHealth); }
    }



}
